import QtQuick 2.9
import QtWebEngine 1.4
import QtQuick.Layouts 1.3

Item {
    property int authDialogWidth: 400
    property Component browserDialogComponent: QML_BrowserDialog {
        //onClosing: destroy()
    }

    function createDialog(profile) {
        var newDialog = browserDialogComponent.createObject();
        newDialog.currentWebView.profile = profile;
        return newDialog;
    }
    id: webDialogsLayout
    WebEngineView{
        id: mainWebView
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left : parent.left
        anchors.right: dialogWebView.left

        url: "http://localhost:11000/"

        Component.onCompleted: {
            profile.clearHttpCache()
        }

        settings.javascriptCanOpenWindows: true
        settings.autoLoadImages: true
        settings.javascriptEnabled: true
        settings.errorPageEnabled: true
        settings.pluginsEnabled: true
        settings.fullScreenSupportEnabled: true
        settings.autoLoadIconsForPage: true
        settings.touchIconsEnabled: true

        onNewViewRequested: {
            if (request.destination == WebEngineView.NewViewInDialog) {
                //var dialog = parent.createDialog(profile);
                //request.openIn(dialog.currentWebView);
                dialogWebView.width = authDialogWidth
                request.openIn(dialogWebView)
            }
        }
    }

    WebEngineView{
        id: dialogWebView
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 0

        Behavior on width {
             NumberAnimation {
                 duration: 200
             }
        }

        onUrlChanged: {
           var urlString = url.toString()
           if (urlString.indexOf("https://ulogin.ru/auth.php?name=google&code=") === 0) // Кастыль для закрывания диалога после ответа uLogin
               dialogWebView.width = 0

           //mainWebView.runJavaScript("test()", function(result) { console.log(result); });
        }

    }
}
