import QtQuick 2.0
import VideoStoreModul 1.0

VideoStore {
    id: root
    MouseArea{
        property bool visibleCursor: true
        cursorShape: visibleCursor ? Qt.PointingHandCursor : Qt.BlankCursor
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            console.log(soundStore.state)
            console.log(soundStore.PlayingState)
            if (root.state === root.playingState())
                root.pause()
            else {
                root.play() // TODO перегрузить как в eduEnReader
            }
        }
        onMouseXChanged:
            if (visibleCursor === false)
                visibleCursor = true
            else
                hideMouseTimer.restart()
        Timer{
            id: hideMouseTimer
            interval: 2000
            onTriggered: parent.visibleCursor = false
        }
    }
    Component.onCompleted: {
        mainController.soundStore = root
    }
}
