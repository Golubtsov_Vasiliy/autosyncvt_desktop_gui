import QtQuick 2.9
import QtWebEngine 1.4


Item {

//    property Component browserWindowComponent: QML_BrowserPage{
//        applicationRoot: root
//        onClosing: destroy()
//    }
    property Component browserDialogComponent: QML_BrowserDialog {
        onClosing: destroy()
    }
    function createDialog(profile) {
        var newDialog = browserDialogComponent.createObject();
        newDialog.currentWebView.profile = profile;
        return newDialog;
    }

    function load(url) {
        webEngineView.url = url
    }

    WebEngineView {
        id: webEngineView
        focus: true
        anchors.fill: parent

        settings.javascriptCanOpenWindows: true
        settings.autoLoadImages: true
        settings.javascriptEnabled: true
        settings.errorPageEnabled: true
        settings.pluginsEnabled: true
        settings.fullScreenSupportEnabled: true
        settings.autoLoadIconsForPage: true
        settings.touchIconsEnabled: true


        states: [
            State {
                name: "FullScreen"
                PropertyChanges {
                    target: tabs
                    frameVisible: false
                    tabsVisible: false
                }
                PropertyChanges {
                    target: navigationBar
                    visible: false
                }
            }
        ]

        onLinkHovered: {
            if (hoveredUrl == "")
                resetStatusText.start();
            else {
                resetStatusText.stop();
                statusText.text = hoveredUrl;
            }
        }


        onCertificateError: {
            error.defer();
            sslDialog.enqueue(error);
        }
        onRenderProcessTerminated: {
            var status = "";
            switch (terminationStatus) {
            case WebEngineView.NormalTerminationStatus:
                status = "(normal exit)";
                break;
            case WebEngineView.AbnormalTerminationStatus:
                status = "(abnormal exit)";
                break;
            case WebEngineView.CrashedTerminationStatus:
                status = "(crashed)";
                break;
            case WebEngineView.KilledTerminationStatus:
                status = "(killed)";
                break;
            }

            print("Render process exited with code " + exitCode + " " + status);
            reloadTimer.running = true;
        }

        onWindowCloseRequested: {
            if (tabs.count == 1)
                browserWindow.close();
            else
                tabs.removeTab(tabs.currentIndex);
        }

        url: "http://localhost:11000/"

        onNewViewRequested: {
            if (request.destination == WebEngineView.NewViewInDialog) {
                var dialog = parent.createDialog(profile);
                request.openIn(dialog.currentWebView);
            }
        }

    //    Timer {
    //        id: reloadTimer
    //        interval: 0
    //        running: false
    //        repeat: false
    //        onTriggered: currentWebView.reload()
    //    }
    }
}
