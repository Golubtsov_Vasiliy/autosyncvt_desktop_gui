import QtQuick 2.0
import QtQuick.Window 2.2
import QtWebEngine 1.4

Window {
    id: rootWindow
    property alias currentWebView: webView
    flags: Qt.Dialog
    width: 800
    height: 600
    visible: true
    onClosing: destroy()
    WebEngineView {
        id: webView
        anchors.fill: parent

        onUrlChanged: {
            var urlString = url.toString()
            if (urlString.indexOf("https://ulogin.ru/auth.php?name=google&code=") === 0) // Кастыль для закрывания диалога после ответа uLogin
                rootWindow.destroy()
        }
    }
}
