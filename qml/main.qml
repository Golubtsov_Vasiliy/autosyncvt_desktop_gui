import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import MainControllerModul 1.0
import VideoStoreModul 1.0
import TextStoreModul 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    MainController{
        id: mainController
        document: TextStore{
            id: mainTextStore

            target: page_workView.textView.textArea
            cursorPosition: target.cursorPosition
            selectionStart: target.selectionStart
            selectionEnd: target.selectionEnd
            onTextChanged: target.text = text
            textColor: "Black"

            fileUrl: "file:///C:/build-autoSyncVT/release/scala.txt"
        }
    }


    SwipeView {
        id: swipeView
        anchors.fill: parent
//        currentIndex: tabBar.currentIndex

        QML_PageWebUI {
            id: page_wevView
        }
        QML_PageWorkView{
            id: page_workView
        }
    }

//    footer: TabBar {
//        id: tabBar
//        currentIndex: swipeView.currentIndex
//        TabButton {
//            text: qsTr("First")
//        }
//        TabButton {
//            text: qsTr("Second")
//        }
//    }
}
