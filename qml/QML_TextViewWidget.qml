import QtQuick 2.0
import QtQuick.Controls 2.2

Flickable{
    id: root
    property var sourceDocument
    property var textArea: frickableTextArea
    property bool readOnly: true

    Component.onCompleted: contentItem.interactive = false
    property bool allowSellectOnlyWord: true
    onVisibleChanged: {
        if (fullScreen === 2)
            hideControlPanel()
    }

    Behavior on contentY {
         NumberAnimation {
             duration: 600
         }
     }

    flickableDirection: Flickable.VerticalFlick
    TextArea.flickable: TextArea {
        id: frickableTextArea
        Accessible.name: "document"
        textFormat: Qt.RichText
        text: sourceDocument.text
        wrapMode: TextArea.Wrap
        focus: true
        selectByMouse: true
        selectByKeyboard: true
        persistentSelection: true
        onSelectionStartChanged: {
            sourceDocument.setSelectionStart(selectionStart);
        }
        onSelectionEndChanged: {
            sourceDocument.setSelectionEnd(selectionEnd);
        }

        //baseUrl: "qrc:/"
        leftPadding: 10
        rightPadding: 10
        topPadding: 0
        bottomPadding: 0
        background: null
        readOnly: root.readOnly
        mouseSelectionMode : TextInput.SelectWords

        onBaseUrlChanged: {
            frickableRoot.setCurY(0)
        }

        // f_pushSynch - синхронизировать независимо от lastMidY
        function syncSoundAndSliderPosition(f_pushSynch)
        {
            if (uiController.dontSynch || uiController.isExample)
                return;

            var mid = uiController.getMidMarkable();
            var midY = positionToRectangle(mid).y;
            if (Math.abs(lastMidY - midY) < minDY && f_pushSynch === false)
                return;
            lastMidY = midY;
            if (midY < 0)
                midY = 0;

            var begin = uiController.getBeginMarkable();
            var end = uiController.getEndMarkable();
            var beginY = positionToRectangle(begin).y;
            var endY = positionToRectangle(end).y;

            var thisYBegin = frickableRoot.getCurY();
            var thisHeight = frickableRoot.getCurHeigth();
            var thisYEnd = thisYBegin + thisHeight - cynhBottpmPading;

            if (endY - beginY >= thisHeight - cynhTopPading)
                frickableRoot.setCurY(beginY - cynhTopPading)
            else
            {
                if (beginY <= thisYBegin)
                    frickableRoot.setCurY(beginY - cynhTopPading)
                if (endY > thisYEnd && endY - thisHeight + cynhBottpmPading > thisYBegin)
                    frickableRoot.setCurY(endY - thisHeight + cynhBottpmPading)
            }
            uiController.markCurText()
        }
        function sellectCurWord(){
            var curPos = textArea.positionAt(mouseArea.mouseX, mouseArea.mouseY)
            sourceDocument.setSelectionByWord(curPos)
        }

        MouseArea{
            acceptedButtons: Qt.LeftButton
            anchors.fill: frickableTextArea
            id : mouseArea
            Component.onCompleted: contentItem.interactive = false
            onPressed: {
                if (frickableTextArea.bindEditingBegin || frickableTextArea.bindEditingEnd)
                    frickableTextArea.stopBindEdting()

                var clickedTextPos = frickableTextArea.positionAt(mouseX, mouseY)
                sourceDocument.setCursorPosition(clickedTextPos);
                //mainController.setTimePosInCursorPos()
                mouse.accepted = false
            }
        }

        onLinkActivated: Qt.openUrlExternally(link)
    }
    ScrollBar.vertical: ScrollBar{
        width: 20
    }

}
