import QtQuick 2.9
import VideoStoreModul 1.0

Item {
    property var textView: textView
    Rectangle{
        id: videoView
        anchors.topMargin: 10
        anchors.leftMargin: 10
        anchors.left: parent.left
        anchors.top: parent.top

        height: parent.height * 2 / 3;
        width: parent.width * 2 / 3 - videoControlPanel.height;

        color: "black"
        QML_VideoWidget{
            id: soundStore
            anchors.fill: parent
            url: "file:///C:/build-autoSyncVT/release/scala.mp4"
        }

    }

    Rectangle{
        id: videoControlPanel
        anchors.margins: 10
        anchors.bottom: textView.top
        anchors.left: parent.left

        width: videoView.width
        height: 50

        color: "lightsteelblue"
    }

    Rectangle{
        id: textView
        property var textArea: textWidget.textArea
        anchors.margins: 10
        anchors.left: parent.left
        anchors.top: videoView.bottom
        anchors.bottom: parent.bottom

        width: videoView.width

        color: "gray"

        QML_TextViewWidget{
            id: textWidget
            anchors.fill:  parent
            sourceDocument: mainController.document
        }
    }

    Rectangle{
        id: rightTopControlPanel
        anchors.margins: 10
        anchors.left: videoView.right
        anchors.top: parent.top
        anchors.right: parent.right

        height: videoView.height

        color: "blue"
    }

    Rectangle{
        id: rightBottomControlPanel
        anchors.margins: 10
        anchors.left: videoView.right
        anchors.right: parent.right
        anchors.top: videoView.bottom
        anchors.bottom: parent.bottom

        color: "green"
    }

}
