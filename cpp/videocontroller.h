#ifndef VIDEOCONTROLLER_H
#define VIDEOCONTROLLER_H

#include <QObject>
#include "cpp/subcontroller.h"

class VideoController : public QObject, public SubController<VideoController>
{
    Q_OBJECT
public:
    explicit VideoController(QObject *parent = nullptr);

signals:

public slots:

protected:
    friend class SubController<VideoController>;

    explicit VideoController(GuiSoundStore::PTR sound, GuiTextStore::PTR text, GuiLogic::PTR logic)
        : SubController(sound, text, logic),
        QObject()
    {}

};

#endif // VIDEOCONTROLLER_H
