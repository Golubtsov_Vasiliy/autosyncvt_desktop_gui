#ifndef TEXTCONTROLLER_H
#define TEXTCONTROLLER_H

#include <QObject>
#include "cpp/subcontroller.h"

class TextController : public QObject, public SubController<TextController>
{
    Q_OBJECT
public:

signals:

public slots:

protected:
    friend class SubController<TextController>;

    explicit TextController(GuiSoundStore::PTR sound, GuiTextStore::PTR text, GuiLogic::PTR logic)
        : SubController(sound, text, logic),
        QObject()
    {}

};

#endif // TEXTCONTROLLER_H
