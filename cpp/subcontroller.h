#ifndef SUBCONTROLLER_H
#define SUBCONTROLLER_H

#include <QObject>
#include <memory>
#include "guilogic.h"
#include "guisoundstore.h"
#include "guitextstore.h"

template <class ControllerType>
class SubController
{
public:
    typedef std::shared_ptr <ControllerType> PTR;
    static PTR factoryMethod(GuiSoundStore::PTR sound, GuiTextStore::PTR text, GuiLogic::PTR logic)
    {
        PTR rezPtr = std::shared_ptr<ControllerType>(new ControllerType(sound, text, logic));
        return rezPtr;
    }

signals:

public slots:

protected:
    GuiSoundStore::PTR _soundStore;
    GuiTextStore::PTR _textStore;
    GuiLogic::PTR _logic;

protected:
    SubController(GuiSoundStore::PTR sound, GuiTextStore::PTR text, GuiLogic::PTR logic)
    {
        _soundStore = sound;
        _textStore = text;
        _logic = logic;
    }
};

#endif // SUBCONTROLLER_H
