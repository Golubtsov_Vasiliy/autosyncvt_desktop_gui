#ifndef GUILOGIC_H
#define GUILOGIC_H

#include "autosyncvt/sources/cpp/baselogic.h"

class GuiLogic : public BaseLogic
{
public:
    typedef std::shared_ptr <GuiLogic> PTR;
    static PTR factoryMethod()
    {
        PTR rezPtr = std::shared_ptr<GuiLogic>(new GuiLogic());
        return rezPtr;
    }



private:
    GuiLogic();

};

#endif // GUILOGIC_H
