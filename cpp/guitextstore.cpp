#include "guitextstore.h"

GuiTextStore::GuiTextStore(QObject *parent) :
    QObject(parent),
    TextStore()
  , _target(0)
  , _doc(&_default_m_doc)
  , _cursorPosition(0)
  , _selectionStart(0)
  , _selectionEnd(0)
  , _mousePresed(false)
  , _someOpened(false)
{
    _markColor = QColor(Qt::blue);
}

QRegExp GuiTextStore::getRegXFor(QString word) const
{
//    QString postFix = Settings::get(Settings::postfix, Settings::Examples).toString();
//    if (postFix.length() != 0)
//        postFix = "(" + postFix + "|\\W" + ")";
//    else
//        postFix = "\\W";
    QRegExp wordRX("\\W" + word.toLower() /*+ postFix*/);
    return wordRX;
}

QList <qint64> GuiTextStore::getWordPositions(const QString& word) const
{
    QList <qint64> rezList;
    QString lowetWord = word.toLower();
    QString lowerText = _text.toLower();
    QRegExp wordRegX = GuiTextStore::getRegXFor(lowetWord);
    qint64 curPos = -1;
    while (true)
    {
        curPos = lowerText.indexOf(wordRegX, curPos + 1);
        if (curPos == -1)
            break;
        rezList.push_back(curPos + 1);
    }
    return rezList;
}

QString GuiTextStore::getString(qint64 begin, qint64 end) const
{
    QString str = getString();
    //Q_ASSERT(end >= begin); // TODO Вернуть после введения понятия бесконечного бинда
    if (end < begin)
        end = begin = 0; // TODO а это стереть

    //if (begin == 5174)
   //     return str.mid(begin, end - begin);

    return str.mid(begin, end - begin);
}

QString GuiTextStore::getString() const
{
    QString planeText = _doc->toPlainText(); // заменить на член
    return planeText;
}

QString GuiTextStore::getSellectedStreing() const
{
    qint64 begin = _selectionStart;
    qint64 end = _selectionEnd;
    QString sellectedString = getString(begin, end);
    return sellectedString;
}

void GuiTextStore::setTarget(QQuickItem *target)
{
    _doc = 0;
    _target = target;
    if (!_target)
        return;

    QVariant doc = _target->property("textDocument");
    if (doc.canConvert<QQuickTextDocument*>()) {
        QQuickTextDocument *qqdoc = doc.value<QQuickTextDocument*>();
        if (qqdoc)
            _doc = qqdoc->textDocument();
    }
    Q_ASSERT(_doc != 0);
    emit targetChanged();
}

void GuiTextStore::setFileUrl(const QUrl &url)
{
    QString fileName = QQmlFile::urlToLocalFileOrQrc(url);
    //assert(fileName.isEmpty() == false);
    _documentTitle = QFileInfo(fileName).baseName();
    _fileUrl = url;
    if (QFile::exists(fileName)) {
        QFile file(fileName);
        if (file.open(QFile::ReadOnly))
        {
            QByteArray data = file.readAll();
            QTextCodec *codec = QTextCodec::codecForName("utf-8");
            setText(codec->toUnicode(data));
            if (_doc)
            {
                _doc->setHtml(data);
                _doc->setModified(false);
                setCursorPosition(1);
            }
            emit textChanged();
            emit documentTitleChanged();
           // _someOpened = true;
           // emit someOpenedChanged();
            reset();
        }
    }
    else if (_doc)
        _doc->clear();

    emit fileUrlChanged();
}

QString GuiTextStore::documentTitle() const
{
    return _documentTitle;
}

void GuiTextStore::setDocumentTitle(QString arg)
{
    if (_documentTitle != arg) {
        _documentTitle = arg;
        emit documentTitleChanged();
    }
}

void GuiTextStore::setText(const QString &arg)
{
    if (_text != arg) {
        _text = arg;
        emit textChanged();
    }
}

void GuiTextStore::saveAs(const QUrl &arg, const QString &fileType)
{
    bool isHtml = fileType.contains(QLatin1String("htm"));
    QLatin1String ext(isHtml ? ".html" : ".txt");
    QString localPath = arg.toLocalFile();
    if (!localPath.endsWith(ext))
        localPath += ext;
    QFile f(localPath);
    if (!f.open(QFile::WriteOnly)) {
        emit error(tr("Cannot save: ") + f.errorString());
        return;
    }
    //QString out = (isHtml ? _doc->toHtml() : _doc->toPlainText()).toUtf8();
  //  _doc->setProperty("lang", QVariant("ru-RU"));
    f.write(_doc->toHtml().toUtf8());
    f.close();
    setFileUrl(QUrl::fromLocalFile(localPath));
}

void GuiTextStore::save()
{
    saveAs(_fileUrl, ".html");
}

QUrl GuiTextStore::fileQUrl() const
{
    return _fileUrl;
}

QString GuiTextStore::text() const
{
    return _text;
}

void GuiTextStore::setCursorPosition(int position)
{
    if (position == _cursorPosition)
        return;

    if (position < 0)
        _cursorPosition = 0;
    else
        _cursorPosition = position;

    reset();
}

void GuiTextStore::reset()
{
    emit fontFamilyChanged();
    emit alignmentChanged();
    emit boldChanged();
    emit italicChanged();
    emit underlineChanged();
    emit fontSizeChanged();
    emit textColorChanged();
    emit cursorPositionChanged();
    emit someSellectedChanged();
}

void GuiTextStore::setSelectionByWord(qint32 pos)
{
    if (pos < 0)
        pos = 0;
    QTextCursor cursor = textCursor();
    cursor.setPosition(pos);
    cursor.select(QTextCursor::WordUnderCursor);

    setSelectionEnd(cursor.selectionEnd());
    setSelectionStart(cursor.selectionStart());
}

QTextCursor GuiTextStore::textCursor() const
{
    QTextCursor cursor = QTextCursor(_doc);
    if (_selectionStart != _selectionEnd &&
            _selectionStart>=0 && _selectionEnd>=0)
    {
        cursor.setPosition(_selectionStart);
        cursor.setPosition(_selectionEnd, QTextCursor::KeepAnchor);
    }
    else if (_cursorPosition >= 0)
        cursor.setPosition(_cursorPosition);
    else
        cursor.setPosition(0);

    return cursor;
}

void GuiTextStore::mergeFormatOnAll(const QTextCharFormat &format)
{
    QTextCursor cursor = textCursor();
    cursor.select(QTextCursor::Document);
    cursor.mergeCharFormat(format);
}

void GuiTextStore::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);
    cursor.mergeCharFormat(format);
}

void GuiTextStore::setSelectionStart(int position)
{
    if (position>0)
        _selectionStart = position;
    else
        _selectionStart = 0;
    emit selectionStartChanged();
    emit someSellectedChanged();
}

void GuiTextStore::setSelectionEnd(int position)
{
    if (position > 0)
        _selectionEnd = position;
    else
        _selectionEnd = 0;
    emit selectionEndChanged();
    emit someSellectedChanged();
}

void GuiTextStore::setAlignment(Qt::Alignment a)
{
    if (_selectionStart < 0 || _selectionEnd < 0)
        return;
    QTextBlockFormat fmt;
    fmt.setAlignment((Qt::Alignment) a);
    QTextCursor cursor = QTextCursor(_doc);
    cursor.setPosition(_selectionStart, QTextCursor::MoveAnchor);
    cursor.setPosition(_selectionEnd, QTextCursor::KeepAnchor);
    cursor.mergeBlockFormat(fmt);
    emit alignmentChanged();
}

Qt::Alignment GuiTextStore::alignment() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return Qt::AlignLeft;
    return textCursor().blockFormat().alignment();
}

bool GuiTextStore::bold() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontWeight() == QFont::Bold;
}

bool GuiTextStore::italic() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontItalic();
}

bool GuiTextStore::underline() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontUnderline();
}

void GuiTextStore::setBold(bool arg)
{
    QTextCharFormat fmt;
    fmt.setFontWeight(arg ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(fmt);
    emit boldChanged();
}

void GuiTextStore::setItalic(bool arg)
{
    QTextCharFormat fmt;
    fmt.setFontItalic(arg);
    mergeFormatOnWordOrSelection(fmt);
    emit italicChanged();
}

void GuiTextStore::setUnderline(bool arg)
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(arg);
    mergeFormatOnWordOrSelection(fmt);
    emit underlineChanged();
}

int GuiTextStore::fontSize() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return 0;
    QTextCharFormat format = cursor.charFormat();
    return format.font().pointSize();
}

void GuiTextStore::setFontSize(int arg)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    QTextCharFormat format;
    format.setFontPointSize(arg);
    mergeFormatOnWordOrSelection(format);
    emit fontSizeChanged();
}

QColor GuiTextStore::textColor() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QColor(Qt::black);
    QTextCharFormat format = cursor.charFormat();
    return format.foreground().color();
}

void GuiTextStore::setTextColor(const QColor &c)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    QTextCharFormat format;
    format.setForeground(QBrush(c));
    mergeFormatOnWordOrSelection(format);
    emit textColorChanged();
}

void GuiTextStore::setWorldMark(const QString& word)
{
    QStringList wordSplit = word.split(QRegExp("\\W"));
    qint64 wordNumber = wordSplit.size();
    setAllUnMarkText();
    auto wordPositions = getWordPositions(word);
    QString lowerText = _text.toLower();
    for (auto pos : wordPositions)
    {
        qint64 posEnd = lowerText.indexOf(QRegExp("\\W"), pos);
        for (int i = 0; i<wordNumber-1; i++)
        {
            posEnd = lowerText.indexOf(QRegExp("\\w"), posEnd);
            posEnd = lowerText.indexOf(QRegExp("\\W"), posEnd);
        }
        setSelectionEnd(posEnd);
        setSelectionStart(pos);
        setMarkText(); // Возвращать старый селлект нет смысла, так как метод исполбьзуется только во время примеров
    }
}

void GuiTextStore::setAllMarkText()
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    QTextCharFormat format;
    format.setBackground(QBrush(_markColor));

    mergeFormatOnAll(format);
}

void GuiTextStore::setMarkText()
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    QTextCharFormat format;
    format.setBackground(QBrush(_markColor));
    mergeFormatOnWordOrSelection(format);
}

void GuiTextStore::setAllUnMarkText()
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    QTextCharFormat format;
    format.setBackground(QBrush(Qt::GlobalColor::white));

    mergeFormatOnAll(format);
}

void GuiTextStore::setUnMarkText()
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    QTextCharFormat format;
    format.setBackground(QBrush(Qt::GlobalColor::white));
    mergeFormatOnWordOrSelection(format);
}

QString GuiTextStore::fontFamily() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QString();
    QTextCharFormat format = cursor.charFormat();
    return format.font().family();
}

void GuiTextStore::setFontFamily(const QString &arg)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    QTextCharFormat format;
    format.setFontFamily(arg);
    mergeFormatOnWordOrSelection(format);
    emit fontFamilyChanged();
}

QStringList GuiTextStore::defaultFontSizes() const
{
    // uhm... this is quite ugly
    QStringList sizes;
    QFontDatabase db;
    foreach (int size, db.standardSizes())
        sizes.append(QString::number(size));
    return sizes;
}


