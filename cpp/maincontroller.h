#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>
#include "textcontroller.h"
#include "videocontroller.h"
#include "webcontroller.h"

class MainController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(GuiTextStore* document READ getDocument WRITE setDocument) // NOTIFY не нужен так как при использовании с ГУИ инициализация идёт в начале,
    Q_PROPERTY(GuiSoundStore* soundStore READ getSoundStore WRITE setSoundStore) // иначе нотификация не нужна.

public:
    explicit MainController(QObject *parent = nullptr);


signals:

public slots:
    void setDocument(GuiTextStore*);
    void setSoundStore(GuiSoundStore*);

    GuiTextStore* getDocument();
    GuiSoundStore* getSoundStore();


protected:
    TextController::PTR _textController;
    VideoController::PTR _videoController;
    WebController::PTR _webController;

    GuiSoundStore::PTR _sound;
    GuiTextStore::PTR _text;
    GuiLogic::PTR _logic;

    // Нужны для контроля над последовательностью инициализации объектов
    void tryInitAllStoreDependedObjects();
    bool storeInited();

};

#endif // MAINCONTROLLER_H
