#ifndef GUISOUNDSTORE_H
#define GUISOUNDSTORE_H

#include <QObject>
#include <cpp/autosyncvt/sources/cpp/soundfragment.h>
#include <VLCQtCore/Common.h>
#include <VLCQtQml/QmlVideoPlayer.h>

class GuiSoundStore : public VlcQmlVideoPlayer, public SoundStore
{
    Q_OBJECT
public:
    GuiSoundStore(QObject *parent = nullptr);

    virtual ~GuiSoundStore() {
       // qDebug() << "~SoundStore()";
    }

signals:

public slots:
    qint32 playingState() { return Vlc::Playing; }
    qint32 bufferingState() { return Vlc::Buffering; }

};

#endif // GUISOUNDSTORE_H
