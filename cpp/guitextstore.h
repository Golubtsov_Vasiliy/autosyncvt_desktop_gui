#ifndef GUITEXTSTORE_H
#define GUITEXTSTORE_H

#include <QObject>
#include <QQuickTextDocument>
#include <QMouseEvent>
#include <QtGui/QTextCharFormat>
#include <QtCore/QTextCodec>
#include <QtAlgorithms>
#include <QDebug>
#include <QQmlFile>
#include <QFileInfo>
#include <QTextCursor>
#include <QFontDatabase>
#include <cpp/autosyncvt/sources/cpp/textstore.h>

class GuiTextStore : public QObject, public TextStore
{
    Q_OBJECT

    Q_ENUMS(HAlignment)

    Q_PROPERTY(QQuickItem *target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(int cursorPosition READ cursorPosition WRITE setCursorPosition NOTIFY cursorPositionChanged)
    Q_PROPERTY(int selectionStart READ selectionStart WRITE setSelectionStart NOTIFY selectionStartChanged)
    Q_PROPERTY(int selectionEnd READ selectionEnd WRITE setSelectionEnd NOTIFY selectionEndChanged)

    Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor NOTIFY textColorChanged)
    Q_PROPERTY(QString fontFamily READ fontFamily WRITE setFontFamily NOTIFY fontFamilyChanged)
    Q_PROPERTY(Qt::Alignment alignment READ alignment WRITE setAlignment NOTIFY alignmentChanged)

    Q_PROPERTY(bool bold READ bold WRITE setBold NOTIFY boldChanged)
    Q_PROPERTY(bool italic READ italic WRITE setItalic NOTIFY italicChanged)
    Q_PROPERTY(bool underline READ underline WRITE setUnderline NOTIFY underlineChanged)

    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)

    Q_PROPERTY(QStringList defaultFontSizes READ defaultFontSizes NOTIFY defaultFontSizesChanged)

    Q_PROPERTY(QUrl fileUrl READ fileQUrl WRITE setFileUrl NOTIFY fileUrlChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QString documentTitle READ documentTitle WRITE setDocumentTitle NOTIFY documentTitleChanged)

    Q_PROPERTY(QColor markColor READ getMarkColor WRITE setMarkCalor NOTIFY markColorChanged)

    Q_PROPERTY(bool someSellected READ someSellected NOTIFY someSellectedChanged)
    Q_PROPERTY(bool someOpened READ someOpened NOTIFY someOpenedChanged)

public:
    explicit GuiTextStore(QObject *parent = nullptr);

    virtual ~GuiTextStore(){}

    int cursorPosition() const
    {
        if (_cursorPosition < 0)
            return 0;
        return _cursorPosition;
    }
    int selectionStart() const { return _selectionStart; }
    int selectionEnd() const { return _selectionEnd; }

    QString fontFamily() const;

    QColor textColor() const;

    Qt::Alignment alignment() const;
    void setAlignment(Qt::Alignment a);

    bool bold() const;
    bool italic() const;
    bool underline() const;
    int fontSize() const;

    QStringList defaultFontSizes() const;
    QString text() const;

    QString documentTitle() const;

    QQuickItem *target() { return _target; }
    void setTarget(QQuickItem *target);


    QString getString(qint64 begin, qint64 end) const;
    QString getString() const;

signals:
    void targetChanged();
    void cursorPositionChanged();
    void selectionStartChanged();
    void selectionEndChanged();

    void fontFamilyChanged();
    void textColorChanged();
    void alignmentChanged();

    void boldChanged();
    void italicChanged();
    void underlineChanged();

    void fontSizeChanged();
    void defaultFontSizesChanged();

    void fileUrlChanged();

    void textChanged();
    void documentTitleChanged();
    void error(QString message);

    void markColorChanged();

    void someSellectedChanged();

    void someOpenedChanged();

public slots:

    void setSelectionStart(int position);
    void setSelectionEnd(int position);
    void setCursorPosition(int position);
    QString getSellectedStreing() const;
    bool someSellected() { return abs(_selectionEnd - _selectionStart) > 0; }
    void setSelectionByWord(qint32 pos);
    void setBold(bool arg);
    void setItalic(bool arg);
    void setUnderline(bool arg);
    void setFontSize(int arg);
    void setTextColor(const QColor &arg);
    void setMarkText();
    void setAllMarkText();
    void setUnMarkText();
    void setAllUnMarkText();
    void setWorldMark(const QString& word);
    void setFontFamily(const QString &arg);

    void setText(const QString &arg);

    void setDocumentTitle(QString arg);

    void setMarkCalor(const QColor& c){ _markColor = c; }
    QColor getMarkColor() const { return _markColor; }

    bool someOpened() { return _someOpened; }

    void setFileUrl(const QUrl &url);
    QUrl fileQUrl() const;

    void saveAs(const QUrl &arg, const QString &fileType);
    void save();
protected:
    bool _someOpened;
    QQuickItem *_target;
    QTextDocument *_doc;

    QTextDocument _default_m_doc;
    QColor _markColor;
    bool _mousePresed;

    qint32 _cursorPosition;
    qint32 _selectionStart;
    qint32 _selectionEnd;

    QFont _font;
    qint32 _fontSize;
    QUrl _fileUrl;
    QString _text;
    QString _documentTitle;

    void reset();
    QTextCursor textCursor() const;
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);
    void mergeFormatOnAll(const QTextCharFormat &format);
    QList <qint64> getWordPositions(const QString& word) const; // Нужно для подсветки слов

    QRegExp getRegXFor(QString word) const;
};

#endif // GUITEXTSTORE_H
