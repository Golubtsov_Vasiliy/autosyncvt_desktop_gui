#ifndef WEBCONTROLLER_H
#define WEBCONTROLLER_H

#include <QObject>
#include "subcontroller.h"

class WebController : public QObject, public SubController<WebController>
{
    Q_OBJECT
public:
    explicit WebController(QObject *parent = nullptr);

signals:

public slots:

protected:
    friend class SubController<WebController>;

    explicit WebController(GuiSoundStore::PTR sound, GuiTextStore::PTR text, GuiLogic::PTR logic)
        : SubController(sound, text, logic),
        QObject()
    {}
};

#endif // WEBCONTROLLER_H
