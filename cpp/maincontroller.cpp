#include "maincontroller.h"

MainController::MainController(QObject *parent) : QObject(parent)
{

}

bool MainController::storeInited()
{
    if (_text.get() == nullptr)
        return false;
    if (_sound.get() == nullptr)
        return false;
    return true;
}

void MainController::tryInitAllStoreDependedObjects()
{
    if (storeInited() == false)
        return;

    _logic = GuiLogic::factoryMethod();
    _textController = TextController::factoryMethod(_sound, _text, _logic);
    _videoController = VideoController::factoryMethod(_sound, _text, _logic);
    _webController = WebController::factoryMethod(_sound, _text, _logic);
}

void MainController::setDocument(GuiTextStore* newTextStore)
{
    _text = GuiTextStore::PTR(newTextStore);
    tryInitAllStoreDependedObjects();
}

void MainController::setSoundStore(GuiSoundStore* newSoundStore)
{
    _sound = GuiSoundStore::PTR(newSoundStore);
    tryInitAllStoreDependedObjects();
}

GuiTextStore* MainController::getDocument()
{
    return (GuiTextStore*)_text.get();
}

GuiSoundStore* MainController::getSoundStore()
{
    return (GuiSoundStore*)_sound.get();
}
