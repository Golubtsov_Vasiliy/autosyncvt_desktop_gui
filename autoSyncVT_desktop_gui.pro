qtHaveModule(widgets): QT += widgets
QT += qml quick quickcontrols2
QT += multimedia multimediawidgets
QT += webengine
QT += webenginewidgets

CONFIG += c++11
CONFIG += qt plugin

#manjaro
#LIBS += -lsndfile
#LIBS += -L/usr/local/lib64/ -lVLCQtCore -lVLCQtQml

#suse
#LIBS += -lsndfile
#LIBS += -lVLCQtCore -lVLCQtQml

#windows
INCLUDEPATH += C:\vlcqt\include
LIBS += -lC:\vlcqt\lib\VLCQtCore -lC:\vlcqt\lib\VLCQtQml

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
FORMS +=

SOURCES += \
    main.cpp \
    cpp/maincontroller.cpp \
    cpp/webcontroller.cpp \
    cpp/videocontroller.cpp \
    cpp/textcontroller.cpp \
    cpp/filecontroller.cpp \
    cpp/subcontroller.cpp \
    cpp/guilogic.cpp \
    cpp/guisoundstore.cpp \
    cpp/guitextstore.cpp \
    cpp/autosyncvt/sources/cpp/baselogic.cpp \
    cpp/autosyncvt/sources/cpp/fragment.cpp \
    cpp/autosyncvt/sources/cpp/logic.cpp \
    cpp/autosyncvt/sources/cpp/soundfragment.cpp \
    cpp/autosyncvt/sources/cpp/soundstore.cpp \
    cpp/autosyncvt/sources/cpp/store.cpp \
    cpp/autosyncvt/sources/cpp/textfragment.cpp \
    cpp/autosyncvt/sources/cpp/textstore.cpp \
    cpp/autosyncvt/sources/cpp/Binding/asr.cpp \
    cpp/autosyncvt/sources/cpp/Binding/bindmaker.cpp \
    cpp/autosyncvt/sources/cpp/Binding/datapreparation.cpp \
    cpp/autosyncvt/sources/cpp/Binding/graph.cpp \
    cpp/autosyncvt/sources/cpp/Binding/metrics.cpp \
    cpp/autosyncvt/sources/cpp/Binding/scripter.cpp \
    cpp/autosyncvt/sources/cpp/Binding/wavworker.cpp \
    cpp/autosyncvt/sources/cpp/Utils/dirworker.cpp \
    cpp/autosyncvt/sources/cpp/Utils/exeptionhandler.cpp \
    cpp/autosyncvt/sources/cpp/Utils/fileworker.cpp \
    cpp/autosyncvt/sources/cpp/Utils/stringutils.cpp

HEADERS += \
    cpp/maincontroller.h \
    cpp/webcontroller.h \
    cpp/videocontroller.h \
    cpp/textcontroller.h \
    cpp/filecontroller.h \
    cpp/subcontroller.h \
    cpp/guilogic.h \
    cpp/guisoundstore.h \
    cpp/guitextstore.h \
    cpp/autosyncvt/sources/cpp/baselogic.h \
    cpp/autosyncvt/sources/cpp/fragment.h \
    cpp/autosyncvt/sources/cpp/logic.h \
    cpp/autosyncvt/sources/cpp/soundfragment.h \
    cpp/autosyncvt/sources/cpp/soundstore.h \
    cpp/autosyncvt/sources/cpp/store.h \
    cpp/autosyncvt/sources/cpp/textfragment.h \
    cpp/autosyncvt/sources/cpp/textstore.h \
    cpp/autosyncvt/sources/cpp/Binding/asr.h \
    cpp/autosyncvt/sources/cpp/Binding/bindmaker.h \
    cpp/autosyncvt/sources/cpp/Binding/datapreparation.h \
    cpp/autosyncvt/sources/cpp/Binding/graph.h \
    cpp/autosyncvt/sources/cpp/Binding/metrics.h \
    cpp/autosyncvt/sources/cpp/Binding/scripter.h \
    cpp/autosyncvt/sources/cpp/Binding/wavworker.h \
    cpp/autosyncvt/sources/cpp/Utils/dirworker.h \
    cpp/autosyncvt/sources/cpp/Utils/exceptionHandler.h \
    cpp/autosyncvt/sources/cpp/Utils/fileworker.h \
    cpp/autosyncvt/sources/cpp/Utils/stringutils.h


