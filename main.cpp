#ifdef QT_WIDGETS_LIB
#include <QApplication>
#define Application QApplication
#else
#include <QGuiApplication>
#define Application QGuiApplication
#endif
#include <QQmlApplicationEngine>
#include <QTextCodec>
#include <QFontDatabase>
#include <QtWebEngine>

#include <VLCQtCore/Common.h>
#include <VLCQtQml/QmlVideoPlayer.h>

#include "cpp/guilogic.h"
#include "cpp/guisoundstore.h"
#include "cpp/guitextstore.h"
#include "cpp/textcontroller.h"
#include "cpp/maincontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    Application app(argc, argv);
    DirWorker::initDurWorker(argc, argv);

    Application::setApplicationName("autoSyncVT_web_UI");
    Application::setOrganizationName("gva-error");

    QQmlApplicationEngine engine;

    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(":/fonts/fontello.ttf") == -1)
        qWarning() << "Failed to load fontello.ttf";
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    QUrl appPath(QString("%1").arg(app.applicationDirPath()));
    engine.rootContext()->setContextProperty("appPath", appPath);

    QtWebEngine::initialize();
    VlcCommon::setPluginPath(app.applicationDirPath() + "/plugins");
    VlcQmlVideoPlayer::registerPlugin();

    qmlRegisterType<GuiSoundStore>("VideoStoreModul", 1, 0, "VideoStore");
    qmlRegisterType<GuiTextStore>("TextStoreModul", 1, 0, "TextStore");
    qmlRegisterType<MainController>("MainControllerModul", 1, 0, "MainController");

    engine.load(QUrl(QLatin1String("qrc:/qml/main.qml")));

//    GuiLogic::PTR logic = GuiLogic::factoryMethod();
//    GuiSoundStore::PTR sound = GuiSoundStore::factoryMethod();
//    GuiTextStore::PTR text = GuiTextStore::factoryMethod();
//    logic->readFromFile("C:\\builded-autoSyncVT\\debug\\hidgab.bnd" , text, sound);

//    TextController::PTR testController = TextController::factoryMethod(sound, text, logic);

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
